#ifndef foocachehfoo
#define foocachehfoo

/* radix-tree (owner) --> rb-tree (rr-type) --> list (rr) */
typedef struct StatelessCache StatelessCache;

StatelessCache* stateless_cache_new(const StatelessNameserver* ns);
void stateless_cache_free(StatelessCache** sc);
void stateless_cache_depp_free(StatelessCache** sc);

/* Insert into cache. Data is instantly cache owned (cleans up on failure) */
StatelessStatus stateless_cache_insert(StatelessCache* sc, ldns_rr* rr);
StatelessStatus stateless_cache_insert_list(StatelessCache* sc, ldns_rr_list* rr_list);

const ldns_rr* stateless_cache_find(const StatelessCache* sc, ldns_rr* rr);
const ldns_rr_list* stateless_cache_find_rrset(const StatelessCache* sc, const ldns_rdf* owner, const ldns_rr_type type);
ldns_rr_list* stateless_cache_find_any(const StatelessCache* sc, const ldns_rdf* owner);

StatelessStatus stateless_cache_delete(StatelessCache* sc, const ldns_rr* rr);
StatelessStatus stateless_cache_delete_rrset(StatelessCache* sc, const ldns_rdf* owner, const ldns_rr_type type);
StatelessStatus stateless_cache_delete_any(StatelessCache* sc, const ldns_rdf* owner);

#endif
