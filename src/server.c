#define _GNU_SOURCE // required for ipv6

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/in6.h>
#include <resolv.h>
#include <fcntl.h>
#include <errno.h>

#include <ldns/ldns.h>
#include <glib.h>

#include "internal.h"
#include "status.h"
#include "stateless.h"
#include "nameserver.h"
#include "pkt_manager.h"

struct StatelessServer {
    StatelessServerConfig c;

    int fd_udp_ipv4;
    int fd_udp_ipv6;

    /* List to availabel interfaces */
    LIST_HEAD(interfaces, StatelessInterface) interfaces;

    /*  */

    regex_t query_regex;

    StatelessServerCallback callback;
    void* userdata;

    StatelessPktManager* pkt_manager;

    struct sockaddr_storage* dns_server;

    GHashTable* hash_nameserver;

    ldns_buffer* wire; // Buffer to generate wireformat of packages
};

/* Needed as modul global because of the avahi-daemon architecture */
static int fd_udp_ipv4 = -1;
static int fd_udp_ipv6 = -1;

/* ldns/example/ldns-testns.c */
static int bind_port(int sock, int port, int fam) {
    struct sockaddr_in addr;
    (void)fam; // prevent unused warnings
#if defined(AF_INET6) && defined(HAVE_GETADDRINFO)
    if(fam == AF_INET6) {
        struct sockaddr_in6 addr6;
        memset(&addr6, 0, sizeof(addr6));
        addr6.sin6_family = AF_INET6;
        addr6.sin6_port = (in_port_t)htons((uint16_t)port);
#  if HAVE_DECL_IN6ADDR_ANY
        addr6.sin6_addr = in6addr_any;
#  else
        memset(&addr6.sin6_addr, 0, sizeof(addr6.sin6_addr));
#  endif
        return bind(sock, (struct sockaddr *)&addr6, (socklen_t) sizeof(addr6));
    }
#endif

#ifndef S_SPLINT_S
    addr.sin_family = AF_INET;
#endif
    addr.sin_port = (in_port_t)htons((uint16_t)port);
    addr.sin_addr.s_addr = INADDR_ANY;
    return bind(sock, (struct sockaddr *)&addr, (socklen_t) sizeof(addr));
}

static int reuse_addr(int fd) {
    int yes;

    yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        stateless_log_error("SO_REUSEADDR failed: %s", strerror(errno));
        return -1;
    }

#ifdef SO_REUSEPORT
    yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(yes)) == -1) {
        stateless_log_error("SO_REUSEPORT failed: %s", strerror(errno));
        return -1;
    }
#endif

    return 0;
}

static int enable_ip_pktinfo(int fd) {
    int yes = 1;

    if (setsockopt(fd, IPPROTO_IP, IP_PKTINFO, &yes, sizeof(yes)) == -1) {
        stateless_log_error("IP_PKTINFO failed: %s", strerror(errno));
        return -1;
    }
    return 0;
}

static int set_file_control(int fd, int attribute) {
    int n;

    if ((n = fcntl(fd, F_GETFL)) < 0)
        return -1;

    if (n & attribute)
        return 0;

    return fcntl(fd, F_SETFL, n|attribute);

}

static StatelessServerConfig init_config(void) {
    StatelessServerConfig config;

    config.echo_domain = strdup(STATELESS_ECHO_DOMAIN);
    config.nameserver = NULL;
    config.use_ipv4 = 1;
    config.use_ipv6 = 0;

    return config;
}

static void copy_config(StatelessServerConfig* dst, const StatelessServerConfig* src) {
    dst->echo_domain = src->echo_domain ? strdup(src->echo_domain) : NULL;
    dst->nameserver = src->nameserver;
    dst->use_ipv4 = src->use_ipv4;
    dst->use_ipv6 = src->use_ipv6;
}

int stateless_open_sockets(const StatelessServerConfig* config) {
    StatelessServerConfig c = config ? *config : init_config();

    if (c.use_ipv4) {
        if ((fd_udp_ipv4 = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
            stateless_log_error("socket() failed: %s", strerror(errno));
            goto fail;
        }

        if (enable_ip_pktinfo(fd_udp_ipv4) < 0)
            goto fail;

        if (0 && reuse_addr(fd_udp_ipv4) < 0) // TODO reuse port?
            goto fail;

        if (bind_port(fd_udp_ipv4, STATELESS_DNS_PORT, AF_INET)) {
            stateless_log_error("bind_port() failed: %s", strerror(errno));
            goto fail;
        }

        if (set_file_control(fd_udp_ipv4, FD_CLOEXEC)) { /* Close on exit */
            stateless_log_error("FD_CLOEXEC failed: %s", strerror(errno));
            goto fail;
        }

        if (set_file_control(fd_udp_ipv4, O_NONBLOCK)) { /* No blocking */
            stateless_log_error("O_NONBLOCK failed: %s", strerror(errno));
            goto fail;
        }
    }

    if (c.use_ipv6) {
        stateless_log_error("Warning: IPv6 currently not supported");
        if (!c.use_ipv4) {
            stateless_log_error("Error: IPv4 mAndatory at the moment");
            return -1;
        }
    }

    return 0;

fail:
    if (fd_udp_ipv4 > 0)
        close(fd_udp_ipv4);
    if (fd_udp_ipv6 > 0)
        close(fd_udp_ipv6);
    fd_udp_ipv4 = fd_udp_ipv6 = -1;

    return -1;
}


StatelessStatus stateless_server_send(StatelessServer* s, ldns_pkt* pkt, const struct sockaddr_storage* addr) {
    ldns_status lstatus;

    assert(s);
    assert(pkt);

    lstatus = ldns_pkt2buffer_wire(s->wire, pkt);
    if (lstatus != LDNS_STATUS_OK) {
        return STATELESS_STATUS_NOMEM;
    }

    /* TODO send with interface addr
     * (http://stackoverflow.com/questions/3062205/setting-the-source-ip-for-a-udp-socket) */
    /* if (!ldns_udp_send_query(s->wire, s->fd_udp_ipv4, &addr->addr, addr->len)) { */
    if (!ldns_udp_send_query(s->wire, s->fd_udp_ipv4, addr, sizeof(struct sockaddr_storage))) {
        return STATELESS_STATUS_UDP_SEND;
    }
    ldns_buffer_clear(s->wire);

    return STATELESS_STATUS_OK;
}

static StatelessStatus set_nameserver(StatelessServer* s) {
    ldns_rdf* rdf;
    size_t unused;
    struct __res_state rs;
    char* addr;

    assert(s);

    if (s->c.nameserver) {
        /* Initialize the dns server socket address */
        rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_A, s->c.nameserver);
        rdf = rdf ? rdf : ldns_rdf_new_frm_str(LDNS_RDF_TYPE_AAAA, s->c.nameserver);
        if (rdf) {
            s->dns_server =  ldns_rdf2native_sockaddr_storage(rdf, 0, &unused);
            ldns_rdf_free(rdf);
            if (!s->dns_server) {
            stateless_log_debug("Set name server address '%s'", s->c.nameserver);
                return STATELESS_STATUS_OK;
            }
            return STATELESS_STATUS_NOMEM;
        }
        stateless_log_warn("Invalid name server address '%s'. Try resolv.conf.", s->c.nameserver);
    }
    
    res_ninit(&rs);
    if (rs.nscount > 0) {
        s->dns_server = calloc(1, sizeof(struct sockaddr_storage));
        memcpy(s->dns_server, &rs.nsaddr_list[0], sizeof(rs.nsaddr_list[0]));

        addr = inet_ntoa(rs.nsaddr_list[0].sin_addr);
        stateless_log_debug("Set name server address '%s'", addr);

        return STATELESS_STATUS_OK;
    }

    return STATELESS_STATUS_ERROR;
}

StatelessStatus stateless_server_send_query(StatelessServer* s, StatelessNameserver* ns, ldns_pkt* pkt, const ldns_rdf* addr) {
    StatelessStatus status;
    StatelessPkt* spkt;
    struct sockaddr_storage* ss;
    size_t unused;

    spkt = stateless_pkt_add(s->pkt_manager, ns, pkt);
    if (!spkt) {
        return STATELESS_STATUS_NOMEM;
    }

    /* Generate socket address or use main one */
    if (addr) {
        ss = ldns_rdf2native_sockaddr_storage(addr, 0, &unused);
        if (!ss) {
            status = STATELESS_STATUS_NOMEM;
        }
        else {
            status = stateless_server_send(s, pkt, ss);
            free(ss);
        }
    }
    else {
        status = stateless_server_send(s, pkt, s->dns_server);
    }

    if (status == STATELESS_STATUS_OK) {
        stateless_log_debug("server_send_query(%%p)", pkt);
        /* Update internal pkt state */
        status = stateless_pkt_update_send(spkt);
        if (status == STATELESS_STATUS_OK) {
            return status;
        }
    }
    
    /* Cleanup if sth went wrong */
    stateless_pkt_free(s->pkt_manager, &spkt);
    return status;
}

static int regex_init(StatelessServer* s) {
    char buffer[1024];
    int regerror;

    assert(s);

    /* Generate regex */
    snprintf(buffer, 1024,  "((.*)\\.(t[0-9]+|00|000)\\.)?((([^\\.]+\\.){1,2})%s)", s->c.echo_domain);
    stateless_log_debug("regcomp(%s)", buffer);
    regerror = regcomp(&s->query_regex, buffer, REG_EXTENDED|REG_ICASE);

    return regerror;
}

StatelessServer* stateless_server_new(const StatelessServerConfig* config, StatelessServerCallback callback, void* userdata, int* error) {
    StatelessStatus status;
    StatelessServer *s;
    int e = 0;

    /* stateless_log_init("/tmp/stateless-log"); */
    stateless_log_init(NULL);
    stateless_log_info("stateless_server_new(%s)", config->echo_domain);

    if (!(s = malloc(sizeof(StatelessServer)))) {
        if (error)
            *error = errno;
        return NULL;
    }
    copy_config(&s->c, config);

    /* Initialize nameserver hashtable */
    s->hash_nameserver = g_hash_table_new(g_str_hash, g_str_equal);
    if (!s->hash_nameserver) {
        if (error)
            *error = ENOMEM;
        free(s);
        return NULL;
    }

    /* Initialize Packet manager */
    s->pkt_manager = stateless_pkt_manager_new();
    if (!s->pkt_manager) {
        if (error)
            *error = ENOMEM;
        g_hash_table_destroy(s->hash_nameserver);
        free(s);
        return NULL;
    }

    s->wire = ldns_buffer_new(LDNS_MAX_PACKETLEN);
    if (!s->wire) {
        /* TODO cleanup */
        return NULL;
    }


    /* Check a previous call to stateless_open_sockets */
    if (fd_udp_ipv4 < 0 && fd_udp_ipv6 < 0) {
        if (stateless_open_sockets(config)) {
            if (error)
                *error = e;
            stateless_pkt_manager_free(&s->pkt_manager);
            g_hash_table_destroy(s->hash_nameserver);
            free(s);
            return NULL;
        }
    }

    /* Initialize the dns name server address */
    status = set_nameserver(s);
    if (status != STATELESS_STATUS_OK) {
        stateless_pkt_manager_free(&s->pkt_manager);
        g_hash_table_destroy(s->hash_nameserver);
        free(s);
        return NULL;
    }

    /* Initialize the regex */
    e = regex_init(s);
    if (e != 0) {
        free(s->dns_server);
        stateless_pkt_manager_free(&s->pkt_manager);
        g_hash_table_destroy(s->hash_nameserver);
        free(s);
        return NULL;
    }

    s->fd_udp_ipv4 = fd_udp_ipv4;
    s->fd_udp_ipv6 = fd_udp_ipv6;

    LIST_INIT(&s->interfaces);

    s->callback = callback;
    s->userdata = userdata;

    return s;
}

void stateless_server_free(StatelessServer **ss) {
    StatelessServer* s;

    assert(ss);
    assert(*ss);
    s = *ss;

    stateless_log_debug("stateless_server_free()");
    stateless_log_close();

    if (s->fd_udp_ipv4 >= 0)
        close(s->fd_udp_ipv4);
    if (s->fd_udp_ipv6 >= 0)
        close(s->fd_udp_ipv6);
    if (s->wire)
        ldns_buffer_free(s->wire);

    free(s);
    *ss = NULL;
}

int stateless_configured(StatelessServer* s) {
    return s->fd_udp_ipv4 >= 0 || s->fd_udp_ipv6 >= 0;
}

int stateless_get_udp_ipv4(StatelessServer* s) {
    return s->fd_udp_ipv4;
}

int stateless_get_udp_ipv6(StatelessServer* s) {
    return s->fd_udp_ipv6;
}

static StatelessMethod str2method(const char* s, size_t n) {
    unsigned int i;

    assert(s);
    assert(n > 0);

    if (s[0] == 't' || s[0] == 'T') {
        if (n < 2) {
            return STATELESS_METHOD_UNKNOWN;
        }
        for (i = 1; i < n; ++i) {
            if (!isdigit(s[i])) {
                return STATELESS_METHOD_UNKNOWN;
            }
        }
        return STATELESS_METHOD_T;
    }
    if (!strncmp("00",s, n)) {
        return STATELESS_METHOD_00;
    }
    return STATELESS_METHOD_UNKNOWN;
}

static StatelessNameserver* ns_lookup(const StatelessServer* s, const char* domain) {
    size_t len;
    StatelessInterface* iface;
    ldns_radix_node_t* node;

    assert(s);
    assert(domain);

    len = strstr(domain, s->c.echo_domain) - domain;
    LIST_FOREACH(iface, &s->interfaces, entries) {
        node = ldns_radix_search(iface->ns, (uint8_t*)domain, len);
        if (node) {
            return node->data;
        }
    }
    return NULL;
}

static StatelessStatus handle_query(StatelessServer* s, ldns_pkt* pkt, const struct sockaddr_storage* css) {
    int qcount;
    ldns_rr* question;
    ldns_rdf* owner;
    char *query = NULL;
    const size_t hits_amount = 5;
    regmatch_t hits[hits_amount];
    int rege;
    char *data, *domain;
    StatelessNameserver* ns;
    StatelessMethod sm;
    StatelessStatus status = STATELESS_STATUS_OK;

    assert(s);
    assert(pkt);

    qcount = ldns_pkt_qdcount(pkt);
    if (qcount == 1) {
        question = ldns_rr_list_rr(ldns_pkt_question(pkt),0);
        owner = ldns_rr_owner(question);
        query = ldns_rdf2str(owner);

        rege = regexec(&s->query_regex, query, hits_amount, hits, 0);
        if (rege) {
            stateless_log_warn("Can't answer question: %%r", question);
            ldns_pkt_set_rcode(pkt, LDNS_RCODE_NXDOMAIN);
        }
        else {
            if (hits[2].rm_so >= 0) {
                /* Stateless query (00,000,nar) */
                data = strndup(&query[hits[2].rm_so], hits[2].rm_eo - hits[2].rm_so);
                sm = str2method(&query[hits[3].rm_so], hits[3].rm_eo - hits[3].rm_so);
            }
            else {
                data = NULL;
                sm = STATELESS_METHOD_UNKNOWN;
            }
            domain = strndup(&query[hits[4].rm_so], hits[4].rm_eo - hits[4].rm_so);


            ns = ns_lookup(s, domain);
            if (ns) {
                status = stateless_nameserver_handle_query(ns, pkt, sm, data, css);
            }
            else {
                stateless_log_warn("No nameserver for '%s'", domain);
                ldns_pkt_set_rcode(pkt, LDNS_RCODE_NXDOMAIN);
            }

            free(data);
            free(domain);
        }

        free(query);
    }

    /* TODO send to local nameserver if desired */
    return status;
}

/* static ldns_pkt* udp_read_wire(int fd, struct sockaddr_storage* from, struct sockaddr_storage* to) { */
/*     uint8_t wire[LDNS_MAX_PACKETLEN]; */
/*     ldns_pkt* pkt; */
/*     ldns_status status; */
/*     ssize_t len; */
/*     struct iovec iov[1]; */
/*     struct msghdr msg; */
/*     union control_data { */
/*         struct cmsghdr  cmsg; */
/*         uint8_t data[CMSG_SPACE(sizeof(struct in_pktinfo))]; */
/*     } cmsg; */
/*     struct cmsghdr *cmsgptr; */
/*     struct sockaddr_in* in; */
/*     struct sockaddr_in6* in6; */

/*     assert(from); */
/*     assert(to); */

/*     /1* http://blog.powerdns.com/2012/10/08/on-binding-datagram-udp-sockets-to-the-any-addresses/ *1/ */
/*     /1* https://groups.google.com/forum/#!original/comp.os.linux.development.system/7Eql8Xkef7o/4W9-jCecneAJ *1/ */
/*     iov[0].iov_base = wire; */
/*     iov[0].iov_len = LDNS_MAX_DOMAINLEN; */

/*     memset(&msg, 0, sizeof(msg)); */
/*     msg.msg_name = &from; */
/*     msg.msg_namelen = sizeof(struct sockaddr_storage); */
/*     msg.msg_iov = iov; */
/*     msg.msg_iovlen = 1; */
/*     msg.msg_control = &cmsg; */
/*     msg.msg_controllen = sizeof(cmsg); */

/*     len = recvmsg(fd, &msg, 0); */
/*     if (len == -1 || len == 0) { */
/*         return NULL; */
/*     } */

/*     /1* Extract destination address *1/ */
/*     for (cmsgptr = CMSG_FIRSTHDR(&msg); cmsgptr != NULL; cmsgptr = CMSG_NXTHDR(&msg, cmsgptr)) { */
/*         if (cmsgptr->cmsg_level == IPPROTO_IP && cmsgptr->cmsg_type == IP_PKTINFO) { */
/*             in = (struct sockaddr_in*)to; */
/*             in->sin_addr = ((struct in_pktinfo*)CMSG_DATA(cmsgptr))->ipi_addr; */
/*             in->sin_family = AF_INET; */
/*             break; */
/*         } */
/*         if (cmsgptr->cmsg_level == IPPROTO_IPV6 && cmsgptr->cmsg_type == IPV6_PKTINFO) { */
/*             in6 = (struct sockaddr_in6*)to; */
/*             in6->sin6_addr = ((struct in6_pktinfo*)CMSG_DATA(cmsgptr))->ipi6_addr; */
/*             in6->sin6_family = AF_INET6; */
/*             break; */
/*         } */
/*     } */

/*     /1* Transform data to pkt *1/ */
/*     status = ldns_wire2pkt(&pkt, wire, len); */
/*     if (status != LDNS_STATUS_OK) { */
/*         return NULL; */
/*     } */
/*     return pkt; */
/* } */

void stateless_event(StatelessServer* s, int fd) {
    struct sockaddr_storage dst;
    socklen_t dst_len;
    ldns_pkt* pkt;
    StatelessPkt* spkt;
    StatelessStatus status;
    int counter = 0;
    uint8_t* raw;
    size_t rawLen;
    ldns_status ls;

    while (1) {
        /* pkt = udp_read_wire(fd, &client.addr, &dst); */
        dst_len = sizeof(struct sockaddr_storage);
        raw = ldns_udp_read_wire(fd, &rawLen, &dst, &dst_len);
        ls = ldns_wire2pkt(&pkt, raw, rawLen);
        /* if (!pkt) { */
        if (LDNS_STATUS_OK != ls) {
            if (counter > 0) {
                stateless_log_debug("Read %d packages in a row...", counter);
            }
            else {
                stateless_log_error("Can't read data from wire");
            }
            break;
        }

        if (ldns_pkt_qr(pkt)) {
            stateless_log_debug("Receive response: %%p", pkt);

            spkt = stateless_pkt_find(s->pkt_manager, pkt);
            if (spkt) {
                /* TODO Error handling */
                stateless_nameserver_response(stateless_pkt_ns(spkt), pkt);
                stateless_pkt_free(s->pkt_manager, &spkt);
            }
        }
        else {
            stateless_log_debug("Receive question: %%p", pkt);
            /* TODO error */
            status = handle_query(s, pkt, &dst);
            if (status != STATELESS_STATUS_OK) {
                stateless_log_error("TODO");
            }
        }

        /* Cleanup */
        ldns_pkt_free(pkt);
        counter++;
    }

}

static char* ns_name_to_domain(StatelessServer* s, const char* ns_name) {
    char buffer[LDNS_MAX_DOMAINLEN];

    assert(s);

    snprintf(buffer, LDNS_MAX_DOMAINLEN, "%s%s%s",
            ns_name,
            ns_name[strlen(ns_name)-1] == '.' ? "" : ".",
            s->c.echo_domain);
    return strdup(buffer);
}


/* Manage interfaces */
static const char* null(const void* ptr) {
    if (!ptr) return "NULL";
    return "OK";
}

StatelessStatus stateless_server_interface_add(StatelessServer* s, const char* addr) {
    StatelessInterface* iface;
    ldns_rdf* rdf;

    if (!s || !addr) {
        stateless_log_error("server_interface_add(%s, %s)", null(s), null(addr));
        return STATELESS_STATUS_NULL;
    }

    /* Test valid addr */
    rdf = ldns_rdf_new_frm_str(LDNS_RDF_TYPE_A, addr);
    if (!rdf) {
        return STATELESS_STATUS_NOMEM;
    }

    /* Test if already there */
    LIST_FOREACH(iface, &s->interfaces, entries) {
        if (!ldns_rdf_compare(rdf, iface->rdf)) {
            return STATELESS_STATUS_OK;
        }
    };

    /* Insert new one */
    iface = malloc(sizeof(StatelessInterface));
    if (!iface) {
        ldns_rdf_free(rdf);
        return STATELESS_STATUS_NOMEM;
    }
    iface->rdf = rdf;

    iface->addr = ldns_rdf2str(rdf);
    if (!iface->addr) {
        free(iface);
        ldns_rdf_free(rdf);
    }

    iface->ns = ldns_radix_create();
    if(!iface->ns) {
        free(iface->addr);
        free(iface);
        ldns_rdf_free(rdf);
    }

    LIST_INSERT_HEAD(&s->interfaces, iface, entries);

    /* TODO Create ns for all known subdomains */
    stateless_log_info("server_interface_add(%s)", iface->addr);
    return STATELESS_STATUS_OK;
}

void stateless_server_interface_remove(StatelessServer* s, const char* addr) {
    if (!s || !addr) {
        stateless_log_error("server_interface_remove(%s, %s)", null(s), null(addr));
        return;
    }
    /* TODO */
}

StatelessStatus stateless_server_interface_update(StatelessServer* s, const char* old_addr, const char* new_addr) {
    if (!s || !old_addr || !new_addr) {
        stateless_log_error("server_interface_update(%s, %s)", null(s), null(old_addr), null(new_addr));
        return STATELESS_STATUS_NULL;
    }
    /* TODO */

    return STATELESS_STATUS_OK;
}

/* Get subdomain one 'normal' or 2 _underscore._doms. */
static char *dub_subdomain(const StatelessServer* s, const ldns_rr* rr) {
    int underscore = 0;
    char* p;
    char* end;
    char* begin = NULL;
    char* ret = NULL;
    char* ownerStr;
    int count = 0;
    ldns_rdf* ownerRdf;

    assert(s);
    assert(rr);

    ownerRdf = ldns_rr_owner(rr);
    if (ownerRdf) {
        ownerStr = ldns_rdf2str(ownerRdf);
        if (ownerStr) {
            end = strstr(ownerStr, s->c.echo_domain);
            if (end) {
                end--; // skip '.'
                for (p = end-1; p >= ownerStr; --p) {
                    if (*p == '.') {
                        if (!underscore || ++count >= 2) {
                            break;
                        }
                        begin = p+1;
                    }
                    else if (*p == '_') {
                        underscore = 1;
                        continue;
                    }
                    underscore = 0;
                }
                begin = p == '\0' ? begin : p+1;

                if (begin && begin < end) {
                    ret = strndup(begin, end-begin);
                }
            }
            free(ownerStr);
        }
    }
    return ret;
}


/* Manage Records */
StatelessStatus stateless_server_record_add(StatelessServer* s, const char* record) {
    ldns_rr* rr;
    ldns_status status;
    StatelessStatus ss;
    StatelessInterface* iface;
    ldns_radix_node_t* node;
    StatelessNameserver* ns;
    char* domain;
    size_t len;
    char* subdom;

    /* Check arguments */
    if (!s || !record) {
        stateless_log_error("server_record_add(%s, %s)", null(s), null(record));
        return STATELESS_STATUS_NULL;
    }

    if (LIST_EMPTY(&s->interfaces)) {
        stateless_log_error("Add record before valid interface");
        return STATELESS_STATUS_IFACE;
    }

    status = ldns_rr_new_frm_str(&rr, record, 0, NULL, NULL);
    if (status != LDNS_STATUS_OK) {
        stateless_log_error("Unsupported record format '%s'", record);
        return STATELESS_STATUS_RR;
    }

    subdom = dub_subdomain(s, rr);
    if (!subdom) {
        stateless_log_error("Unsupported publish domain '%%d'", ldns_rr_owner(rr));
        ldns_rr_free(rr);
        return STATELESS_STATUS_RR;
    }
    stateless_log_debug("Subdomain :: %s", subdom);

    /* Search ns with subdomain per available interface */
    domain = ns_name_to_domain(s, subdom);
    len = strstr(domain, s->c.echo_domain) - domain;
    LIST_FOREACH(iface, &s->interfaces, entries) {
        /* node = ldns_radix_search(iface->ns, (uint8_t*)subdomain, len); */
        node = ldns_radix_search(iface->ns, (uint8_t*)domain, len);
        if (!node) {
            /* Add new ns for subdomain */
            ns = stateless_nameserver_new(s, domain, iface->addr);
            if (!ns) {
                stateless_log_error("Can't create nameserver (%s, %s)", subdom, iface->addr);
                free(domain);
                ldns_rr_free(rr);
                return STATELESS_STATUS_NOMEM;
            }
            /* status = ldns_radix_insert(iface->ns, (uint8_t*)subdomain, len, ns); */
            status = ldns_radix_insert(iface->ns, (uint8_t*)domain, len, ns);
            if (status != LDNS_STATUS_OK) {
                stateless_nameserver_free(&ns);
                free(domain);
                ldns_rr_free(rr);
                return STATELESS_STATUS_NOMEM;
            }
        }
        else {
            ns = node->data;
        }

        /* Insert nameserver */
        ss = stateless_nameserver_add_record(ns, rr);
        if (STATELESS_STATUS_OK != ss) {
            stateless_log_error("server_add_record failed: %s", stateless_status_str(ss));
            ldns_rr_free(rr);
        }
    }


    free(domain);
    return STATELESS_STATUS_OK;
}

void stateless_server_record_remove(StatelessServer* s, const char* record) {
    if (!s || !record) {
        stateless_log_error("server_record_remove(%s, %s)", null(s), null(record));
        return;
    }

    /* TODO */
}
