#ifndef foodnspktmanagerh
#define foodnspktmanagerh

StatelessPktManager* stateless_pkt_manager_new(void);
void stateless_pkt_manager_free(StatelessPktManager** spm);


/* Insert pkt to manager. Also set generated id in pkt */
StatelessPkt* stateless_pkt_add(
    StatelessPktManager* spm,
    StatelessNameserver* ns,
    ldns_pkt* pkt);

/* Find Package to response */
StatelessPkt* stateless_pkt_find(StatelessPktManager* spm, const ldns_pkt* response);

/* Remove and destroy */
void stateless_pkt_free(StatelessPktManager* spm, StatelessPkt** pkt);

/* Get ns */
StatelessNameserver* stateless_pkt_ns(StatelessPkt* pkt);

/* Update state */
StatelessStatus stateless_pkt_update_send(StatelessPkt* pkt);

#endif
