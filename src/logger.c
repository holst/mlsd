#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "internal.h"
#include "logger.h"

static const char* log_level_str[] = {
    "DEBUG",
    "INFO",
    "WARN",
    "ERROR"
};

#define LOG_BUF_SIZE 8196
static char buffer[LOG_BUF_SIZE];

static int fd = -1;
static int close_fd = 0;

static StatelessLog level = STATELESS_LOG_DEBUG;

void stateless_log_init(const char* filename) {
    if (fd >= 0) {
        stateless_log_warn("Logfile already initialized");
    }
    else if (!filename) {
        fd = STDERR_FILENO;
        stateless_log_info("Log to stderr");
    }
    else {
        fd = open(filename, O_CREAT|O_WRONLY|O_APPEND, 0666);
        if (fd < 0) {
            stateless_log_error("Can't open file '%s'. Fallback to stderr.", filename);
            fd = STDERR_FILENO;
            stateless_log_info("Log to stderr");
        }
        else {
            close_fd = 1;
            stateless_log_info("Log to '%s'", filename);
        }
    }

    stateless_log_debug("Current loglevel: %s", log_level_str[level]);
}

void stateless_log_close(void) {
    write(fd, "-------------------\n", 20); // tmporary to indicate new end
    if (close_fd) {
        close(fd);
    }
}


/* Set/Get current loglevel */
StatelessLog stateless_log_level(void) {
    return level;
}

void stateless_log_set_level(StatelessLog l) {
    if (l <= level) {
        stateless_log_info("Change loglevel to %s", log_level_str[l]);
        level = l;
    }
}

static size_t insert_time(char *buf, size_t len) {
    struct timeval tv;
    struct tm *tm;
    size_t n, ret = 0;

    gettimeofday(&tv, NULL);
    tm = localtime(&tv.tv_sec);

    n = strftime(buf, len, "%Y-%m-%d %H:%M:%S", tm);
    if (n && n < len) {
        ret = n;
        buf += n;

        n = snprintf(buf, len-n, ".%03ld", tv.tv_usec / 1000);
        ret += n;
    }
    return ret;
}

static size_t append_rdf(ldns_buffer* lbuf, const ldns_rdf* rdf) {
    size_t pos = lbuf->_position;

    assert(lbuf);
    assert(rdf);

    ldns_rdf2buffer_str(lbuf, rdf);

    return lbuf->_position - pos;
}

static size_t append_rr(ldns_buffer* lbuf, const ldns_rr* rr) {
    size_t pos = lbuf->_position;

    assert(lbuf);
    assert(rr);

    ldns_rr2buffer_str(lbuf, rr);
    lbuf->_position--; // without new line

    return lbuf->_position - pos;
}

static size_t append_rr_list(ldns_buffer* lbuf, const ldns_rr_list* rr_list) {
    size_t pos = lbuf->_position;
    size_t i, n;

    assert(lbuf);
    assert(rr_list);

    ldns_buffer_write(lbuf, "(", 1);
    n = ldns_rr_list_rr_count(rr_list);
    for (i = 0; i < n; ++i) {
        append_rr(lbuf, ldns_rr_list_rr(rr_list, i));
        if (i < n-1) {
            ldns_buffer_write(lbuf, ", ", 2);
        }
    }
    ldns_buffer_write(lbuf, ")", 1);

    return lbuf->_position - pos;
}


/* Mostly from the ldns library */
static size_t append_pkt_header(ldns_buffer *lbuf, const ldns_pkt *pkt) {
    ldns_lookup_table *opcode;
    ldns_lookup_table *rcode;
    size_t pos = lbuf->_position;

    assert(lbuf);
    assert(pkt);

    ldns_buffer_printf(lbuf, "#%d:", ldns_pkt_id(pkt));

    opcode = ldns_lookup_by_id(ldns_opcodes, ldns_pkt_get_opcode(pkt)); //;opc);
    rcode = ldns_lookup_by_id(ldns_rcodes,ldns_pkt_get_rcode(pkt));

    if (!ldns_pkt_qr(pkt) && opcode) {
        /* ldns_buffer_printf(lbuf, "Q-%s: ", opcode->name); */
        ldns_buffer_write(lbuf, opcode->name, 1);
        ldns_buffer_write(lbuf, ":", 1);
    }
    if (ldns_pkt_qr(pkt) && rcode) {
        ldns_buffer_printf(lbuf, "%s:", rcode->name);
    }

    /* if (ldns_pkt_qr(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "qr "); */
    /* } */
    /* if (ldns_pkt_aa(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "aa "); */
    /* } */
    /* if (ldns_pkt_tc(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "tc "); */
    /* } */
    /* if (ldns_pkt_rd(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "rd "); */
    /* } */
    /* if (ldns_pkt_cd(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "cd "); */
    /* } */
    /* if (ldns_pkt_ra(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "ra "); */
    /* } */
    /* if (ldns_pkt_ad(pkt)) { */
    /*     ldns_buffer_printf(lbuf, "ad "); */
    /* } */

    return lbuf->_position - pos;
}

static size_t append_pkt(ldns_buffer* lbuf, const ldns_pkt* pkt) {
    size_t pos = lbuf->_position;
    size_t i;
    const ldns_rr_list* sections[4] = {
        ldns_pkt_qr(pkt) ? NULL : ldns_pkt_question(pkt),
        ldns_pkt_answer(pkt),
        ldns_pkt_additional(pkt),
        ldns_pkt_authority(pkt)
    };
    const char* section_name[] = {"Qst", "Ans", "Add", "Ath"};

    assert(lbuf);
    assert(pkt);

    ldns_buffer_write(lbuf, "Pkg:", 4);
    (void)append_pkt_header(lbuf, pkt);
    /* n = snprintf(idbuffer, 10, "#%d", ldns_pkt_id(pkt)); */
    /* ldns_buffer_write(lbuf, idbuffer, n); */

    ldns_buffer_write(lbuf, "(", 1);
    for (i = 0; i < 4; ++i) {
        if(sections[i] && ldns_rr_list_rr_count(sections[i]) > 0) {
            ldns_buffer_write(lbuf, section_name[i], 3);
            append_rr_list(lbuf, sections[i]);
        }
    }
    ldns_buffer_write(lbuf, ")", 1);

    return lbuf->_position - pos;
}

static size_t vsnprintf_ldns(char* buf, size_t orig_len, const char* fmt, va_list ap) {
    char* pb = buf;
    const char* fp = fmt;
    int special = 0; // set to 1 if a % is found in fmt
    size_t n, len = orig_len;

    /* Small hack to get the ldns write funciton without allocations */
    ldns_buffer lbuf;
    lbuf._data = (uint8_t*)buf;
    lbuf._capacity = orig_len;
    lbuf._limit = orig_len;
    lbuf._position = 0;
    lbuf._fixed = 1;
    lbuf._status = LDNS_STATUS_OK;

    for (; *fp && len; ++fp) {
        if (!special) {
            if (*fp == '%') {
                special = 1;
                continue;
            }
            *pb++ = *fp;
            len--;
        }
        else {
            lbuf._position = orig_len - len;
            switch (*fp) {
                case 'd':
                    n = append_rdf(&lbuf, va_arg(ap, ldns_rdf*));
                    break;
                case 'r':
                    n = append_rr(&lbuf, va_arg(ap, ldns_rr*));
                    break;
                case 'l':
                    n = append_rr_list(&lbuf, va_arg(ap, ldns_rr_list*));
                    break;
                case 'p':
                    n = append_pkt(&lbuf, va_arg(ap, ldns_pkt*));
                    break;
                case '%':
                default:
                    *pb = *fp;
                    n = 1;
            }
            special = 0;

            if (n < len) {
                pb += n;
                len -= n;
            }
        }
    }

    return orig_len - len;
}

static void stateless_log(StatelessLog l, const char* fmt, va_list ap) {
    char fmt_buffer[4096];
    char* p = buffer;
    size_t n, len = LOG_BUF_SIZE;

    if (fd < 0) {
        fd = STDERR_FILENO;
        stateless_log_warn("Logged before initialize logger (Log temporary to stderr)");
        stateless_log(l, fmt, ap);
        fd = -1;
        return;
    }

    if (l < level) {
        /* Nothing to do */
        return;
    }

    /* Insert current time */
    n = insert_time(p, len);
    if (n < len) {
        p += n;
        len -= n;
    }

    /* Insert log level */
    n = snprintf(p, len, "\t%6s\t", log_level_str[l]);
    if (n < len) {
        p += n;
        len -= n;
    }

    (void)vsnprintf(fmt_buffer, 4096, fmt, ap);
    n = vsnprintf_ldns(p, len, fmt_buffer, ap);

    /* /1* Format string *1/ */
    /* n = vsnprintf(p, len, fmt, ap); */
    if (n && n < len) {
        p += n;
        len -= n;
    }

    /* Add new line */
    *p = '\n';
    len--;


    /* Write to log */
    (void)write(fd, buffer, LOG_BUF_SIZE - len);

    va_end(ap);
}

/* Log functions */
void stateless_log_debug(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    stateless_log(STATELESS_LOG_DEBUG, fmt, ap);
}

void stateless_log_info(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    stateless_log(STATELESS_LOG_INFO, fmt, ap);
}

void stateless_log_warn(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    stateless_log(STATELESS_LOG_WARN, fmt, ap);
}

void stateless_log_error(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    stateless_log(STATELESS_LOG_ERROR, fmt, ap);
}
