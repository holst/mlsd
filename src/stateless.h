#ifndef foostatelesshfoo
#define foostatelesshfoo

#include "status.h"

typedef struct StatelessServer StatelessServer;

typedef void (*StatelessServerCallback) (StatelessServer *s, void* userdata);

typedef struct StatelessServerConfig {
    char *echo_domain; /* Set NULL to use default one */
    char *nameserver; /* Set NULL to use first in resolv.conf */
    int use_ipv4;
    int use_ipv6;
} StatelessServerConfig;


/* These functions only exists to handle the initialization with the avahi-daemon
 * It drops root privileges before initializ the mdns-server */
int stateless_open_sockets(const StatelessServerConfig* config);
int stateless_get_udp_ipv4(StatelessServer* s);
int stateless_get_udp_ipv6(StatelessServer* s);


StatelessServer* stateless_server_new(
        const StatelessServerConfig* config,
        StatelessServerCallback callback,
        void* userdata,
        int* error
        );
void stateless_server_free(StatelessServer **s);

int stateless_configured(StatelessServer* s);

void stateless_event(StatelessServer* s, int fd);

/* Manage interfaces */
StatelessStatus stateless_server_interface_add(StatelessServer* s, const char* addr);
void stateless_server_interface_remove(StatelessServer* s, const char* addr);
StatelessStatus stateless_server_interface_update(
        StatelessServer* s,
        const char* old_addr,
        const char* new_addr);

/* Manage Records
 * <name>.<subdomain>.@  IN  <type>  <data> */
StatelessStatus stateless_server_record_add(
        StatelessServer* s,
        const char* record);
void stateless_server_record_remove(
        StatelessServer* s,
        const char* record);

#endif
